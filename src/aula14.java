public class aula14 {  // constantes

    public static void main(String[] args) {
        /*
        1) os dados nunca serão alterados
        2) uma constante tem o seu endereço protegido
        3) uma constante nunca será desalocada
        4) declaramos uma vez, utilizamos quantas vezes for preciso
         */

              int var = 10; //declara uma variável
        final int constante = 10; // final, int tipo inteiro constante

        //var = 1001;
        //constante = 80;


        System.out.println( var ); // imprime a variavel
        System.out.println( constante ); // imprime a constante
    }
}
